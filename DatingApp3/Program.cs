﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;

namespace DatingApp
{
    class Program : SqlCommands
    {

        public static void Main(string[] args)
        {
            MainMenu();
        }


        public static void MainMenu()
        {
            Console.WriteLine("Main Menu");
            Console.WriteLine("------------");
            Console.WriteLine("[1] Opret Bruger");
            Console.WriteLine("[2] Login");
            Console.WriteLine("[3] Tilbage");
            Console.WriteLine("[4] Afslut");
            Console.WriteLine("------------");
            Console.WriteLine("Vælg imellem 1-4");

            string valgmulighed = Console.ReadLine();
            int nummer;
            bool result = Int32.TryParse(valgmulighed, out nummer);
            if (result)
            {
                Console.Clear();
                SubMenu(nummer);

            }
            else
            {
                Console.WriteLine("Forkert valg!");
            }
        }

        public static void SubMenu(int mainMenuChoices)
        {
            switch (mainMenuChoices)
            {
                case 1:
                    Console.WriteLine("[1] Opret bruger");
                    break;
                case 2:
                    Console.WriteLine("[2] Login");
                    break;
                case 3:
                    Console.WriteLine("[3] Tilbage");
                    break;
                case 4:
                    Console.WriteLine("[4] Exit");
                    break;
            }
            string choice = Console.ReadLine();
            int nummer;
            bool result = Int32.TryParse(choice, out nummer);
            if (result)
            {
                Action(mainMenuChoices, nummer);
            }
            else
            {
                Console.WriteLine("Forkert valg");
                Console.ReadLine();
            }
            
        }
        public static void Action(int menu, int choice)
        {


            switch (menu)
            {
                case 1:
                    switch (choice)
                    {
                        case 1:
                            var instance = new SqlCommands();
                            instance.opretBruger();
                            Console.Clear();
                            MainMenu();
                            return;                                         
                        case 2:
                            //Gør noget
                            break;
                        case 3:
                            Console.Clear();
                            MainMenu();
                            break;
                        case 4:
                            Environment.Exit(0);
                            break;
                    }
                    break;

               case 2:
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("TeST");
                                
                            break;

                        case 2:
                            //Gør noget
                            break;

                        case 3:
                            Console.Clear();
                            MainMenu();
                            break;
                        case 4:
                            Environment.Exit(0);
                            break;
                    }
                    break;

                case 3:
                    switch (choice)
                    {
                        case 1:
                            //tilføj kode her
                            break;
                        case 2:
                            //tilføj kode her
                            break;
                        case 3:
                            Console.Clear();
                            MainMenu();
                            break;
                        case 4:
                            Environment.Exit(0);
                            break;


                    }
                    break;
            }
        }

    }

}


    

